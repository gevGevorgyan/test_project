'use strict';

const _omit = require('lodash/omit');

module.exports = (sequelize, DataTypes) => {
    const Content = sequelize.define(
        'Content',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            content: {
                allowNull: false,
                type: DataTypes.STRING,
            }
        },
        {
            tableName: 'content',
            timestamps: true
        }
    );

    Content.prototype.fields = async function() {
        return _omit(this.get(), []);
    };

    return Content;
};
