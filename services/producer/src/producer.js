'use strict';

const _ = require('underscore');
const express = require('express');
const kafka = require('kafka-node');
const bodyParser = require('body-parser');
const { Client: PgClient } = require('pg');
const Router = require('express-promise-router');

const type = require('./type');

const pgClient = new PgClient();
pgClient.connect();

const kafkaClientOptions = { sessionTimeout: 100, spinDelay: 100, retries: 2 };
const kafkaClient = new kafka.Client(process.env.KAFKA_ZOOKEEPER_CONNECT, 'producer-client', kafkaClientOptions);
const kafkaProducer = new kafka.HighLevelProducer(kafkaClient);

kafkaClient.on('error', (error) => console.error('Kafka client error:', error));
kafkaProducer.on('error', (error) => console.error('Kafka producer error:', error));

const app = express();
const router = new Router();

app.use('/', router);
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.post('/testing', (req, res) => {
  const { string } = req.body;

  const messageBuffer = type.toBuffer({
    string: string,
    date: Date.now()
  });

  const payload = [{
    topic: 'test-topic',
    messages: messageBuffer,
    attributes: 1
  }];

  kafkaProducer.send(payload, function(error, result) {
    console.info('Sent payload to Kafka:', payload);

    if (error) {
      console.error('Sending payload failed:', error);
      res.status(500).json(error);
    } else {
      console.log('Sending payload result:', result);
      res.status(202).json(result);
    }
  });
});

router.get('/testing', async (req, res) => {
  const { rows } = await pgClient.query('SELECT * FROM test');
  res.status(200).json(rows);
});

app.listen(process.env.PRODUCER_PORT);
