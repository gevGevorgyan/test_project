const avro = require('avsc');

const avroSchema = {
  name: 'Type',
  type: 'record',
  fields: [
    {
      name: 'date',
      type: {
        type: 'long',
        logicalType: 'timestamp-millis'
      }
    },
    {
      name: 'string',
      type: 'string'
    }
  ]
};

const type = avro.parse(avroSchema)

module.exports = type;
