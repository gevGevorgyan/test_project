CREATE TABLE test (
  id          bigserial,
  uuid        uuid,
  string       varchar  DEFAULT 0.0,
  date   timestamp with time zone NOT NULL,
  created_at  timestamp with time zone NOT NULL DEFAULT NOW()
);
